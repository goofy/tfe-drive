var Sync = function()
{
    this.timer=null;
};


Sync.prototype.init = function()
{
    var self=this;
    window.clearTimeout(this.timer);

    if(settings.getEnablesync())
    {
        this.timer = window.setInterval(this.run.bind(this), settings.getSyncTime()*1000);
    }
};

Sync.prototype.run = function()
{
    if(!online.online)
    {
        return;
    }

    var transaction = files.db.transaction([ 'sync' ]);
    var dbsync = transaction.objectStore('sync');

    // open a cursor to retrieve all items from the 'notes' store
    dbsync.openCursor().onsuccess = function (e) {
        var cursor = e.target.result;
        if (cursor) {
            var sync_item = cursor.value;

            var txt = sync_item.type == 'upload' ? translate('sync_local_to_remote') : translate('sync_remote_to_local');
            txt += '/'+sync_item.source_path+' ->  /'+sync_item.destination_path;

            // Create fake instances
            var files_instance = new Files();
            files_instance.silent_init().then(function()
            {
                var api = new window[sync_item.api_type]();
                api.initDb().then(function()
                {
                    api.setAccount(sync_item.api_id)
                        .then(function() {
                            files_instance.setAccount(sync_item.api_id, api); 
                            files_instance.run_sync(sync_item.id)
                            .then(function()
                            {
                                notif.send(translate('task_done'),txt, 'tfe drive '+sync_item.id);
                            }, function()
                            {
                                console.error('task run error');
                                notif.send(translate('task_error'),txt, 'tfe drive '+sync_item.id);
                            });
                    });
            });
            });

            cursor.continue();
        }
    };
};

