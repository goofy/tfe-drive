var Sdcard = function()
{
    var self=this;
    this.type='sdcard';
    this.cachedir='cache/';
};


Sdcard.prototype.init = function()
{
    var self=this;
    this.prefix=translate('app_title')+'/';
    this.sdcards = navigator.getDeviceStorages('sdcard');
    this.sdcard = this.sdcards[0];

    this.cache = {};
    this.sdcards.forEach(function(sdcard_sub)
    {
        sdcard_sub.onchange= self.update.bind(self);
    });
    this.list_dirs();
};

Sdcard.prototype.update = function(e)
{
    var self=this;
    console.log('sdcard.update!');
    return new Promise(function(ok, reject)
    {
        self.updated=true;
        self.cache={};
        self.list_dirs().then(ok,reject);
    });
};

Sdcard.prototype.getfile = function(path)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var sdcard_selected=null;
        self.sdcards.forEach(function(sdcard_sub)
        {
            var re = new RegExp('^\\/?'+sdcard_sub.storageName);
            if(re.test(path))
            {
                sdcard_selected=sdcard_sub;
                path= path.replace(re,'').replace(/^\/+/,'');
            }
        });
        if(!sdcard_selected)
        {
            console.error('cannot choose which sdcard to use ',path, self.sdcards);
            return reject();
        }

        // Check if available on device
        request = sdcard_selected.get(path);
        request.onsuccess = function() { 
            var file = this.result;
            ok(file);
        };
        request.onerror = reject;
    });
};
Sdcard.prototype.update_settings = function()
{
    var self=this;
    self.cache_enabled = settings.getEnableCache();

    var sdcard_selected=null;
    var storagename = settings.getCacheLocation();

    self.sdcards.forEach(function(sdcard_sub)
    {
        if(sdcard_sub.storageName==storagename)
        {
            sdcard_selected=sdcard_sub;
        }
    });
    if(!sdcard_selected)
    {
        sdcard_selected = self.sdcards[0];
    }
    self.cache_sdcard = sdcard_selected;
};


Sdcard.prototype.cache_image = function(id,url)
{
    var self=this;
    return new Promise(function(ok, reject)
    {

        id = id.replace(/[\/"']/g,'');
        var fetch = self.prefix+self.cachedir+id;
        var fetch_sdcard=  self.cache_sdcard;
        if(/^sdcard:\/\//.test(url))
        {
            //remove sdcard + trailing first slash
            fetch = url.replace(/^sdcard:\/+/,'');
            self.sdcards.forEach(function(sdcard_sub)
            {
                var re = new RegExp('^\\/?'+sdcard_sub.storageName);
                if(re.test(fetch))
                {
                    fetch_sdcard=sdcard_sub;
                    fetch= fetch.replace(re,'').replace(/^\/+/,'');
                }
            });
        }
        // If cache not needed (local file)
        else if(!/^http/.test(url))
        {
            return ok(url);
        }

        // Check if available on device
        request = fetch_sdcard.get(fetch);
        request.onsuccess = function() {
            var file = this.result;
            var mysrc = URL.createObjectURL(file);
            if(file.size)
            {
                ok(mysrc);
            }
            else
            {
                console.error('empty cache image file, fetching new one',file);
                var request = fetch_sdcard.delete(self.prefix+self.cachedir+id);
                request.onsuccess = function()
                {
                    self.cache_image_net(id,url).then(ok, reject);
                };
                request.onerror = function()
                {
                    console.error('cannot delete empty cache image file');
                    reject();
                };
            }
        };
        request.onerror = function(e)
        {
            console.err('error ',e);
            self.cache_image_net(id,url).then(ok, reject);
        };
    });
};

Sdcard.prototype.cache_image_net = function(id,url)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        // if not available, download and save!
        var r = new XMLHttpRequest({ mozSystem: true });
        r.open('GET', url, true);
        r.responseType = "arraybuffer";
        if(files.api.account && (files.api.type=='Dropbox' || files.api.type=='OneDrive' || files.api.type=='Box'))
        {
            r.setRequestHeader("authorization","Bearer "+files.api.account.access_token);
        }
        else if(files.api.account && (files.api.type=='Google'))
        {
            r.setRequestHeader("authorization","OAuth "+files.api.account.access_token);
        }
        else if(files.api.account && files.api.type=='Webdav')
        {
            r.setRequestHeader("Authorization","Basic "+btoa(files.api.account.username+":"+files.api.account.password));
        }
        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                // not available yet, wait a little
                if(r.status ==202)
                {
                    window.setTimeout(function()
                    {
                        self.cache_image_net(id,url).then(ok,reject);
                    }, 2000);
                }
                else if(r.status >= 200 && r.status<400)
                {
                    var arrayBufferView = new Uint8Array( r.response );
                    var blob = new Blob( [ arrayBufferView ], { type: "octet/stream" } );
                    if(!settings.getEnableCache())
                    {
                        var reader = new window.FileReader();
                        reader.readAsDataURL(blob); 
                        reader.onloadend = function() {
                            // return base64 encoded value
                            ok(reader.result);
                        };
                    }
                    else
                    {
                        var request = self.cache_sdcard.addNamed(blob, self.prefix+self.cachedir+id);

                        // If file saved success
                        request.onsuccess = function() {
                            var reader = new window.FileReader();
                            reader.readAsDataURL(blob); 
                            reader.onloadend = function() {
                                // return base64 encoded value
                                ok(reader.result);
                            };
                        };
                        request.onerror = function() {
                            // File already present...
                            if(this.error.name=='NoModificationAllowedError')
                            {
                                var reader = new window.FileReader();
                                reader.readAsDataURL(blob); 
                                reader.onloadend = function() {
                                    // return base64 encoded value
                                    ok(reader.result);
                                };
                            }
                            else
                            {
                                console.error('cannot write cache file with content! ',this, self.cache_sdcard,  self.prefix+self.cachedir+id);
                                reject();
                            }
                        };
                    }
                }
                else
                {
                    reject();
                }
            }
        };
        r.send();
    });
};

Sdcard.prototype.add = function(blob, file , absolute_path)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var sdcard_selected = self.sdcard;
        // Remove storageName from sdcard
        if(absolute_path)
        {
            self.sdcards.forEach(function(sdcard_sub)
            {
                var re = new RegExp('^\\/?'+sdcard_sub.storageName);
                if(re.test(file))
                {
                    sdcard_selected=sdcard_sub;
                }
            });
            if(!sdcard_selected)
            {
                console.error('cannot choose which sdcard to use ',path, self.sdcards);
                return reject();
            }

            var re = new RegExp('^\/?'+sdcard_selected.storageName+'\/');
            file=file.replace(re,'');
        }
        else
        {
            file = self.prefix+file;
        }

        file = file.replace(/\/+/g,'/').replace(/^\//,'');
        console.log('adding file ',file, sdcard_selected);

        // Check if existing file exists
        request = sdcard_selected.delete(file);

        // file existed
        request.onsuccess = function() { 
            var request = sdcard_selected.addNamed(blob, file);
            request.onsuccess = ok;
            request.onerror = function()
            {
                console.error('error addnamed ',this);
                reject();
            };
        };

        // file was not existing, but creating a new one anyway
        request.onerror = function() { 
            var request = sdcard_selected.addNamed(blob,file);
            request.onsuccess = ok;
            request.onerror = function()
            {
                console.error('error addNamed2 ',this);
                reject();
            };
        };
        
    });
};

Sdcard.prototype.delete = function(file , absolute_path)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var sdcard_selected = self.sdcard;
        // Remove storageName from sdcard
        if(absolute_path)
        {
            self.sdcards.forEach(function(sdcard_sub)
            {
                var re = new RegExp('^\\/?'+sdcard_sub.storageName);
                if(re.test(file))
                {
                    sdcard_selected=sdcard_sub;
                }
            });
            if(!sdcard_selected)
            {
                console.error('cannot choose which sdcard to use ',path, self.sdcards);
                return reject();
            }

            var re = new RegExp('^\/?'+sdcard_selected.storageName+'\/');
            file=file.replace(re,'');
        }
        else
        {
            file = self.prefix+file;
        }
        // Check if existing file exists
        var request = sdcard_selected.delete(file);

        // file existed
        request.onsuccess = ok;
        request.onerror = reject;
    });
};

Sdcard.prototype.list_dirs = function(skip_cache)
{
    var self=this;
    var dirs={ parent: null, path:'', dirs: {}};
    return new Promise(function(ok, reject)
    {
        if(self.cache.root && !skip_cache)
        {
            console.log('using sdcard list dirs cache');
            return ok(self.cache.root);
        }
        console.log('no cache!',skip_cache);

        var ended=0;

        self.sdcards.forEach(function(sdcard_sub)
        {
            // Let's browse all the images available
            var cursor = sdcard_sub.enumerate();

            cursor.onsuccess = function() 
            {
                var file = this.result;
                if(file)
                {
                    var name = file.name;
                    var dir = name.replace(/[^\/]+$/,'');
                    var segments = dir.split(/\//);
                    var current_dir = dirs;
                    var current_path = '';
                    for(var i=0; i<segments.length; i++)
                    {
                        var segment = segments[i];
                        if(segment)
                        {
                            if(!current_dir.dirs[segment])
                            {
                                current_dir.dirs[segment] ={ 'path': current_path+segment+'/','parent': current_dir, 'dirs': {} };
                            }
                            current_dir = current_dir.dirs[segment];
                            current_path+=segment+'/';
                        }
                    }
                } 
                // Once we found a file we check if there is other results
                if (!this.done)
                {
                    // Then we move to the next result, which call the cursor
                    // success with the next file as result.
                    this.continue();
                }
                else
                {
                    ended++;
                    if(ended==self.sdcards.length)
                    {
                        self.cache.root = dirs;
                        ok(dirs);
                    }
                }
            };

            cursor.onerror = function()
            {
                ended++;
                console.error('error listing dirs ',sdcard_sub);
                if(ended==self.sdcards.length)
                {
                    self.cache.root = dirs;
                    ok(dirs);
                }
            };
        });
    });
};

Sdcard.prototype.list_files = function(path, fullinfo)
{
    var self=this;
    var files=[];


    path = path.replace(/^\/+/,'');
     var re = new RegExp('^\\/?'+path.replace(/\//g,'\\/')+'[^\\/]+$');
     var notEmpty = new RegExp('\/\.empty');
    return new Promise(function(ok, reject)
    {
        var sdcard_selected=null;
        self.sdcards.forEach(function(sdcard_sub)
        {
            var re = new RegExp('^\\/?'+sdcard_sub.storageName);
            if(re.test(path))
            {
                sdcard_selected=sdcard_sub;
            }
        });
        if(!sdcard_selected)
        {
            if(!path && self.sdcards.length>1)
            {
                return ok([]);
            }
            else
            {
                console.error('cannot choose which sdcard to use ',path, self.sdcards);
                return reject();
            }
        }

        // Let's browse all the images available
        var cursor = sdcard_selected.enumerate();

        cursor.onsuccess = function () {
            var file = this.result;
            if(file)
            {
                if(re.test(file.name) && !notEmpty.test(file.name))
                {
                    if(fullinfo)
                    {
                        files.push(file);
                    }
                    else
                    {
                        files.push(file.name);
                    }
                } 
            }
            // Once we found a file we check if there is other results
            if (!this.done) {
                // Then we move to the next result, which call the cursor
                // success with the next file as result.
                this.continue();
            }
            else
            {
                ok(files);
            }
        };

        cursor.onerror = function()
        {
            console.error('error listing path ',this);
            reject();
        };
    });
};
