var Online = function()
{
    this.url='http://www.mozilla.org/robots.txt';
    this.previous=true;
    this.online=true;
};


Online.prototype.init = function()
{
    window.setInterval(this.loop.bind(this), 1000);
};

Online.prototype.loop = function()
{
    var self=this;
    this.check().then(function()
    {
        self.online=true;
        if(self.previous!==true)
        {
            files.online();
        }
        self.previous=self.online;
    }, function()
    {
        self.online=false;
        if(self.previous!==false)
        {
            files.offline();
        }
        self.previous=self.online;
    });
};


Online.prototype.check = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        if(navigator.onLine)
        {
            ok();
        }
        else
        {
            reject();
        }
    });
};

