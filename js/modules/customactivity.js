var CustomActivity = 
{
    activityReq: null,
    isPicking:false,

    init: function()
    {
    },
    send: function(blob)
    {
        CustomActivity.isPicking = false;
        CustomActivity.activityReq.postResult.type = blob.type;
        CustomActivity.activityReq.postResult({
            type: blob.type,
            name: blob.name,
            blob: blob
        });
    }
};

navigator.mozSetMessageHandler('activity', function (activityReq) {
    var option = activityReq.source;

    if (option.name === "pick") {
        CustomActivity.activityReq = activityReq;
        CustomActivity.isPicking = true;
    }
});

