var Internal = function(){};
Internal.prototype = new Apiprot();

Internal.prototype.icon='cloud';
Internal.prototype.type='Internal';
Internal.prototype.dbname='Internal';
Internal.prototype.star_available = false;
//Internal.prototype.delete_available = false;
Internal.prototype.download_available=false;
//Internal.prototype.move_folder_available=false;
Internal.prototype.upload_available=false;
Internal.prototype.nocache=true;

Internal.prototype.init = function()
{
    var self=this;
    this.accounts = {};

    this.account=
    {
        id: 'internal',
        email: translate('internal_sdcards'),
        type: 'sdcard'
    };

    accounts.add(this.account, this);

    this.initDb();
};

Internal.prototype.setAccount = function(id)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        sdcard.update();
        ok();
    });
};


Internal.prototype.list_dir = function(id)
{
    var self=this;

    return new Promise(function(ok, reject)
    {
        id=id.replace(self.account.id+'_','');
        if(id=='root')
        {
            id='/';
        }
        console.log('list dirs ',id);
        //if(id=='/') { id=''; }

        sdcard.list_dirs().then(function(dirs)
        {
            console.log('received list of dirs!');
            var segments = id.split(/\//);
            var found=true;
            segments.forEach(function(segment)
            {
                if(segment)
                {
                    if(dirs.dirs[segment])
                    {
                        dirs = dirs.dirs[segment];
                    }
                    else
                    {
                        console.error('cannot find segment ',segment);
                        return ok({items:[]});
                    }
                }
            });
            console.log('end of search segment');

            var result = [];
            Object.keys(dirs.dirs).forEach(function(dir)
            {
                result.push(
                {
                    parent: self.account.id+'_'+id,
                    icon: 'img/icons/dropbox/folder.gif',
                    mime: '',
                    creation_date: null,
                    modified_date: null,
                    id: self.account.id+'_'+id+dir+'/',
                    realid: id+dir+'/',
                    name: dir,
                    thumbnail: null,
                    size: 0,
                    download: null,
                    is_folder:  true,
                    is_starred:  false,
                    is_hidden:  false,
                    is_trashed:  false
                });
            });

            sdcard.list_files(id,true).then(function(files)
            {
                files.forEach(function(file)
                {
                    var filename = file.name.replace(/^.*\//,'');
                    var mime =filename.indexOf('.')!==-1 ? filename.replace(/^.*\./,'') : '???';
                    var icon =  'page_white';

                    mimeData = self.getMimeAndIcon(filename);
                    mime = mimeData.mime;
                    icon=mimeData.icon;

                    result.push({
                        parent: self.account.id+'_'+id,
                        icon: 'img/icons/dropbox/'+(icon)+'.gif',
                        mime: mime,
                        creation_date: null,
                        modified_date: file.lastModifiedDate.getTime(),
                        id: self.account.id+'_'+id+filename,
                        realid: id+'/'+filename,
                        size: file.size,
                        name: filename,
                        thumbnail: null,
                        download: id+'/'+filename,
                        is_folder:  false,
                        is_starred:  false,
                        is_hidden:  false,
                        is_trashed:  false
                    });
                });
                
                console.log('result ',result);
                ok({items:result});
            }, reject);
        }, reject);
    });
};

Internal.prototype.open = function(received_item)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        sdcard.getfile(received_item.realid.replace(/^\//,'')).then(function(file)
        {
            file.name=received_item.name;

            if(CustomActivity.isPicking)
            {
                CustomActivity.send(file);
                return ok();
            }

            var activity = new Activity({
                name: 'open',
                data: {
                type: file.type,
                allowSave: false,
                blob: file,
                title: received_item.name
                }
            });
            activity.onsuccess= ok;
            activity.onerror = function() {
                console.error('error ',this.error);
                if(this.error.name==='NO_PROVIDER')
                {
                    reject(translate('no_provider'));
                }
                else
                {
                    console.error('big fail? ',this.error, received_item);
                    reject();
                }
            };
        }, function(err)
        {
            console.error('fail open ',err,this);
            reject();
        });
    });
};

Internal.prototype.rename = function(id, newname, is_folder)
{
    // TODO
    var self=this;
    id=id.replace(self.account.id+'_','');

    return new Promise(function(ok, reject)
    {
        if(!is_folder)
        {
            sdcard.getfile(id).then(function(file)
            {
                dest = id.replace(/[^\/]+\/?$/, newname);
                console.log('move ',file, dest);
                sdcard.add(file, dest, true).then(
                function()
                {
                    sdcard.delete(id,true).then(ok, reject);
                }, reject);
            }, reject);
        }
        else
        {
            console.log('h1')
            var dest = id.replace(/[^\/]+\/?$/, newname+'/');
            console.log('move ',id+' to '+dest);
            self.move(id, dest, true).then(ok, reject);
        }
    });
};

Internal.prototype.delete = function(id, is_folder)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        if(!is_folder)
        {
            sdcard.delete(id,true).then(ok, reject);
        }
        else
        {
            self.list_dir(id).then(function(dirs)
            {
                var promises=[];
                dirs.items.forEach(function(item)
                {
                    promises.push(self.delete(item.id, item.is_folder));
                });
                Promise.all(promises).then(function()
                {
                    self.delete(id).then(ok, reject);
                }, reject);
            });
        }
    });
};

Internal.prototype.share = function(item)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        console.log('opening ',item);
        sdcard.getfile(item.realid.replace(/^\//,'')).then(function(file)
        {
            return ok(file);
        }, function()
        {
            return reject(translate('error_opening_file'));
        });
    });
};

Internal.prototype.move = function(id, destination, is_folder)
{
    // TODO
    var self=this;
    id=id.replace(self.account.id+'_','');
    destination=destination.replace(self.account.id+'_','');
    if(destination=='root')
    {
        destination='/';
    }

    return new Promise(function(ok, reject)
    {
        if(!is_folder)
        {
            sdcard.getfile(id).then(function(file)
            {
                dest = destination+'/'+id.replace(/.*\//, '');
                sdcard.add(file, dest, true).then(
                function()
                {
                    sdcard.delete(id,true).then(ok, reject);
                }, reject);
            }, reject);
        }
        else
        {
            var promises=[];
            var prefix = id.replace(/.*([^\/]+\/)$/,'$1');
            self.list_dir(id).then(function(dirs)
            {
                dirs.items.forEach(function(item)
                {
                    var dest;
                    dest = destination+prefix;

                    console.log('move('+item.id+','+dest+','+item.is_folder+')');
                    promises.push(self.move(item.id, dest, item.is_folder));
                });
                console.log('moving sub dir of ',id, dirs);
            });
            Promise.all(promises).then(function()
            {
                console.log('deleteing ',id);
                self.delete(id).then(ok, reject);
            }, reject);
        }
    });
};

Internal.prototype.copy = function(id, destination, is_folder)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    destination=destination.replace(self.account.id+'_','');

    return new Promise(function(ok, reject)
    {
        if(!is_folder)
        {
            console.log('copy ',id);
            var file = sdcard.getfile(id).then(function(file)
            {
                var name = file.name.replace(/^.*\//,'');
                sdcard.add(file,destination+'/'+name,true).then(ok, reject);
            },reject);
        }
    });
};

Internal.prototype.mkdir = function(id, newname)
{
    var self=this;
    id=id.replace(self.account.id+'_','');



    return new Promise(function(ok, reject)
    {
        var dir =
        {
            parent: self.account.id+'_'+id,
            icon: 'img/icons/dropbox/folder.gif',
            mime: '',
            creation_date: null,
            modified_date: null,
            id: self.account.id+'_'+id+newname+'/',
            realid: id+newname+'/',
            name: newname,
            thumbnail: null,
            size: 0,
            download: null,
            is_folder:  true,
            is_starred:  false,
            is_hidden:  false,
            is_trashed:  false
        };

        var blob = new Blob( [ '' ], { type: 'text/plain'} );
        blob.name='/.empty';
        self.create(dir.id+'/.empty', blob).then(function()
        {
            ok(dir);
        }, reject);
    });
}

Internal.prototype.create = function(id,blob)
{
    id=id.replace(this.account.id+'_','');

    return new Promise(function(ok, reject)
        {
            var dest= id+blob.name.replace(/.*\//,'');
            sdcard.add(blob, dest, true).then(ok, reject);
        });
};



