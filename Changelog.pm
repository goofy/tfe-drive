# 0.0.8

- browse local files
- clear database option in settings
- add donation link

# 0.0.7

- Explicit message when opening a file and the file is not supported by the phone.
- fix copy/move to root directories
- box.com api implementation
- better thumbnail loading (3 by 3)
- accounts list scrollbar if a lot of accounts created

# 0.0.5

- change app icon
- file picker! compatible with google/dropbox/webdav 
- onedrive (microsoft drive) integration
- dropbox implementation
- webdav/owncloud integration
- fix download of google docs (using export links)

# 0.0.4

- Upload is now available for both internal or external cards.
- Enable/Disable cache images
- Choose on which sdcard put the cache files
- Display error if cache file creation fail
- Add warning about uploading individual files (thks mac from foxapps.eu)
- (ALPHA) Auto Sync folder - Upload or Download (every X minutes)

# 0.0.3

- Download folders and sub folders
- ru-RU translation
- better offline mode

# 0.0.2

- Fix settings github links

# 0.0.1

First commit
